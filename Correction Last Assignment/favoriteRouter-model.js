//-- Modulos

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

//-- Models

const Favorites = require('../models/favorite');
const Dishes = require('../models/dishes');
var authenticate = require('../authenticate');

//-- Cors

const cors = require('./cors');

//-- Routes

const favoriteRouter = express.Router();
favoriteRouter.use(bodyParser.json());

//-- ('/')

favoriteRouter.route('/')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    //GET
    .get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        //Coge la lista de favoritos del usuario que realiza la peticiÃ³n
        Favorites.findOne({ user: req.user._id })
            .populate('user')
            .populate('dishes')
            .then((favorites) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(favorites);
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    //POST
    .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        //Busca la lista de favoritos del usuario que realiza la peticiÃ³n
        Favorites.findOne({ user: req.user._id })
            .then((favorite) => {
                //Si existe la lista
                if (favorite) {
                    //Buscamos la posiciÃ³n donde colocarlo
                    for (var i = 0; i < req.body.length; i++) {
                        if (favorite.dishes.indexOf(req.body[i]._id) === -1) {
                            favorite.dishes.push(req.body[i]._id);
                        }
                    }
                    //Lo guardamos
                    favorite.save()
                        .then((favorite) => {
                            console.log('Favorite Created ', favorite);
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }, (err) => next(err));
                }
                //Si no existe la lista
                else {
                    //Creamos la lista
                    Favorites.create({ "user": req.user._id, "dishes": req.body })
                        .then((favorite) => {
                            console.log('Favorite Created ', favorite);
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }, (err) => next(err));
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    //PUT
    .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /favorites');
    })
    //DELETE
    .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        //Buscamos la lista de favoritos del usuario y la eliminamos
        Favorites.findOneAndRemove({ "user": req.user._id })
            .then((resp) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.json(resp);
            }, (err) => next(err))
            .catch((err) => next(err));
    });

//-- ('/:dishId')

favoriteRouter.route('/:dishId')
    .options(cors.corsWithOptions, (req, res) => { res.sendStatus(200); })
    //GET
    .get(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('GET operation not supported on /favorites/' + req.params.dishId);
    })
    //POST
    .post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        //Busca la lista de favoritos del usuario que realiza la peticiÃ³n
        Favorites.findOne({ user: req.user._id })
            .then((favorite) => {
                //Si existe la lista
                if (favorite) {
                    if (favorite.dishes.indexOf(req.params.dishId) === -1) {
                        favorite.dishes.push(req.params.dishId)
                        //Lo guardamos
                        favorite.save()
                            .then((favorite) => {
                                console.log('Favorite Created ', favorite);
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json');
                                res.json(favorite);
                            }, (err) => next(err))
                    }
                }
                //Si no existe la lista
                else {
                    //Creamos la lista
                    Favorites.create({ "user": req.user._id, "dishes": [req.params.dishId] })
                        .then((favorite) => {
                            console.log('Favorite Created ', favorite);
                            res.statusCode = 200;
                            res.setHeader('Content-Type', 'application/json');
                            res.json(favorite);
                        }, (err) => next(err))
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    })
    //PUT
    .put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /favorites/' + req.params.dishId);
    })
    //DELETE
    .delete(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
        //Buscamos la lista de favoritos del usuario y la eliminamos
        Favorites.findOne({ user: req.user._id })
            .then((favorite) => {
                //si existe
                if (favorite) {
                    index = favorite.dishes.indexOf(req.params.dishId);
                    //Si el dish esta en favoritos
                    if (index >= 0) {
                        favorite.dishes.splice(index, 1);
                        favorite.save()
                            .then((favorite) => {
                                console.log('Favorite Deleted ', favorite);
                                res.statusCode = 200;
                                res.setHeader('Content-Type', 'application/json');
                                res.json(favorite);
                            }, (err) => next(err));
                    }
                    //Si el dish no esta en favoritos
                    else {
                        err = new Error('Dish ' + req.params.dishId + ' not found');
                        err.status = 404;
                        return next(err);
                    }
                }
                //Si no existe
                else {
                    err = new Error('Favorites not found');
                    err.status = 404;
                    return next(err);
                }
            }, (err) => next(err))
            .catch((err) => next(err));
    });


module.exports = favoriteRouter;