//-- Modulos

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//-- Schema Favorite

var favoriteSchema = new Schema({
    user:  {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    dishes:  [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Dish'
    }]
}, {
    timestamps: true
});

//-- Export Favorite model

module.exports = mongoose.model('Favorite', favoriteSchema);