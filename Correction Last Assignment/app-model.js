//-- Modulos

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var FileStore = require('session-file-store')(session);
var passport = require('passport');
var authenticate = require('./authenticate');
var config = require('./config');

//-- Conexion mongo

const mongoose = require('mongoose');
const url = config.mongoUrl;
const connect = mongoose.connect(url, { useCreateIndex: true, useNewUrlParser: true });
connect.then((db) => {
  console.log('Connected correctly to server');
}, (err) => {
  console.log(err);
});

//-- Express 1

var app = express();
// Secure traffic only
app.all('*', (req, res, next) => {
  if (req.secure) {
    return next();
  }
  else {
    res.redirect(307, 'https://' + req.hostname + ':' + app.get('secPort') + req.url);
  }
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//-- Passport

app.use(passport.initialize());

//-- Routes principal y user

var indexRouter = require('./routes/index');
app.use('/', indexRouter);

var usersRouter = require('./routes/users');
app.use('/users', usersRouter);

//-- Express 2

app.use(express.static(path.join(__dirname, 'public')));

//-- Enrrutamiento

var dishRouter = require('./routes/dishRouter');
app.use('/dishes', dishRouter);

var promoRouter = require('./routes/promoRouter');
app.use('/promotions', promoRouter);

var leaderRouter = require('./routes/leaderRouter');
app.use('/leaders', leaderRouter);

const uploadRouter = require('./routes/uploadRouter');
app.use('/imageUpload', uploadRouter);

const favoriteRouter = require('./routes/favoriteRouter');
app.use('/favorites', favoriteRouter);

//-- Errores

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;